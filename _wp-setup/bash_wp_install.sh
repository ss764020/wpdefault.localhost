#!/bin/bash
################################################
# @title: WPインストール実行バッシュファイル
# @description:
# - このファイルは基本修正しないこと
# - 万が一修正が必要な場合はbatファイルも修正する
# @author: y.ohta
# @rev: 1.0.3
################################################
cd `dirname $0`
pwd

##### .env ファイル存在確認 ######
if [ ! -f .env ]; then
  echo '[error] ".env" is not exist.'
  exit 0
fi

##### インストール実行確認 ######
read -p "Will you install WordPress? (y/N): " yn
case "$yn" in [yY]*) ;; *) echo "abort." ; exit ;; esac

##### ルートディレクトリへ移動 ######
cd ../

##### 変数読み込み ######
source ./_wp-setup/.env

##### WPコアインストール実行 ######
wp core download --locale=ja
wp core config --dbname=$DBNAME --dbuser=$DBUSER --dbpass=$DBPASS --dbhost=$DBHOST --dbprefix=$DBPREFIX
wp core install --url=$SITEURL --title=$SITETTL --admin_user=$ADMINUSR --admin_password=$ADMINPWD --admin_email=$ADMINMAIL

##### 不要プラグイン削除 ######
wp plugin uninstall hello

##### 任意プラグイン追加 ######
bash ./_wp-setup/bash_wp_setup_plugin.sh

##### テーマの有効化 ######
wp theme activate mytheme

##### 不要テーマの削除 ######
wp theme delete --all

##### トップページを作成 ######
if [[ ! $(wp post list --pagename=home | grep home) ]];then
  wp post create --post_name=home --post_title='トップページ' --post_type=page --post_status=publish
fi

##### フロントページの設定変更 ######
wp option update show_on_front 'page'
if [[ $(wp post list --pagename=home | grep home) =~ ^([0-9]*) ]]
then
  if [ -n "${BASH_REMATCH[0]}" ]
  then
    wp option update page_on_front ${BASH_REMATCH[0]}
  fi
fi

##### デフォルトの「プライバシーポリシー」ページを削除 ######
if [[ $(wp post list --post_type=page --post_status=draft,publish --name="privacy-policy" | grep privacy-policy) =~ ^([0-9]*) ]]
then
  if [ -n "${BASH_REMATCH[0]}" ]
  then
    wp post delete ${BASH_REMATCH[0]}
  fi
fi

##### 初期下層ページを作成 ######
bash ./_wp-setup/bash_wp_setup_page.sh

##### オプション変更 ######
bash ./_wp-setup/bash_wp_setup_option.sh

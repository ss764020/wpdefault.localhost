@ECHO off
SETLOCAL enabledelayedexpansion
REM ################################################
REM # @title: WPカテゴリ追加/削除実行バッシュファイル
REM # @description:
REM # - このファイルは基本修正しないこと
REM # - 万が一修正が必要な場合はbatファイルも修正する
REM # @author: y.ohta
REM # @rev: 1.0.0
REM ################################################
cd /d %~dp0..\

FOR /F "delims=, tokens=1-3 eol=#" %%l IN (_wp-setup\_conf_category.conf) DO (
  set RESULT=

  if %%l == create (
    for /f "usebackq tokens=*" %%i in (`php "c:/wp-cli/wp-cli.phar" term list category --slug^=%%m ^| find "%%m"`) do (
      set RESULT=!RESULT!^

%%i
    )
    if "!RESULT!" == "" (
      set RESULT=!RESULT:~3,3!
      set RESULT=!RESULT: =!

      php "c:/wp-cli/wp-cli.phar" term create category %%n --slug=%%m
    )
  )
  if %%l == delete (
    for /f "usebackq tokens=*" %%i in (`php "c:/wp-cli/wp-cli.phar" term list category --slug^=%%m ^| find "%%m"`) do (
      set RESULT=!RESULT!^

%%i
    )
    if not "!RESULT!" == "" (
      set RESULT=!RESULT:~3,3!
      set RESULT=!RESULT: =!
      wp term delete category !RESULT!
    )
  )
)

REM ##### pause #####
ECHO "[finish] setup category"
PAUSE

@ECHO off
SETLOCAL enabledelayedexpansion
REM ################################################
REM # @title: WP設定変更実行バッチファイル
REM # @description:
REM # - このファイルは基本修正しないこと
REM # - 万が一修正が必要な場合はbatファイルも修正する
REM # @author: y.ohta
REM # @rev: 1.0.0
REM ################################################
cd /d %~dp0..\

for /f "delims=, eol=#" %%l in (_wp-setup\_conf_option.conf) do (
  php "c:/wp-cli/wp-cli.phar" %%l
)

REM ##### pause #####
ECHO "[finish] setup option"
PAUSE

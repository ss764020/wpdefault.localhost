#!/bin/bash
################################################
# @title: WPカテゴリ追加/削除実行バッシュファイル
# @description:
# - このファイルは基本修正しないこと
# - 万が一修正が必要な場合はbatファイルも修正する
# @author: y.ohta
# @rev: 1.0.3
################################################
cd `dirname $0`
IFS=,

cat _conf_category.conf | while read -a line
do
  if [ $? -eq 0 ];then
    if [ ${line[0]} = "create" ];then
      if [[ ! $(wp term list category --slug=${line[1]} | grep ${line[1]}) ]];then
        wp term create category ${line[2]} --slug=${line[1]}
      fi
    fi
    if [ ${line[0]} = "delete" ];then
      if [[ $(wp term list category --slug=${line[1]} | grep ${line[1]}) =~ ^([0-9]*) ]];then
        if [ -n "${BASH_REMATCH[0]}" ]
        then
          wp term delete category ${BASH_REMATCH[0]}
        fi
      fi
    fi
  fi
done

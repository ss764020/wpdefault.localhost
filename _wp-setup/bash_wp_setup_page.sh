#!/bin/bash
################################################
# @title: WP固定ページ作成/削除実行バッシュファイル
# @description:
# - このファイルは基本修正しないこと
# - 万が一修正が必要な場合はbatファイルも修正する
# @author: y.ohta
# @rev: 1.0.3
################################################
cd `dirname $0`
IFS=,

iconv -f cp932 _conf_page.conf | cat | while read -a line
do
  if [ $? -eq 0 ];then
    if [ ${line[0]} = "create" ];then
      if [[ ! $(wp post list --pagename=${line[1]} | grep ${line[1]}) ]];then
        wp post create --post_name=${line[1]} --post_title=${line[2]} --post_type=page --post_status=publish
      fi
    fi
    if [ ${line[0]} = "delete" ];then
      if [[ $(wp post list --pagename=${line[1]} | grep ${line[1]}) =~ ^([0-9]*) ]];then
        if [ -n "${BASH_REMATCH[0]}" ]
        then
          wp post delete ${BASH_REMATCH[0]}
        fi
      fi
    fi
  fi
done

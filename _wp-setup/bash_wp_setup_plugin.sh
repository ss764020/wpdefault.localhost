#!/bin/bash
################################################
# @title: WPプラグインインストール実行バッシュファイル
# @description:
# - このファイルは基本修正しないこと
# - 万が一修正が必要な場合はbatファイルも修正する
# @author: y.ohta
# @rev: 1.0.3
################################################
cd `dirname $0`

iconv -f cp932 _conf_plugin.conf | cat | while read line
do
  echo $line | grep -v '^#.*' > /dev/null
  if [ $? -eq 0 ];then
    wp plugin $line
  fi
done

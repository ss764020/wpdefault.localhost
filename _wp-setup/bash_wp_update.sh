#!/bin/bash
################################################
# @title: WPアップデート実行バッシュファイル
# @description:
# - コア、プラグイン、翻訳を一括更新
# @author: y.ohta
# @rev: 1.0.3
################################################
cd `dirname $0../`

wp core update
wp plugin update --all
wp language core update
wp language plugin update --all

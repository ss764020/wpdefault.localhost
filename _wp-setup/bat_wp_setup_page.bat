@ECHO off
SETLOCAL enabledelayedexpansion
REM ################################################
REM # @title: WP固定ページ作成/削除実行バッチファイル
REM # @description:
REM # - このファイルは基本修正しないこと
REM # - 万が一修正が必要な場合はbatファイルも修正する
REM # @author: y.ohta
REM # @rev: 1.0.0
REM ################################################
cd /d %~dp0

FOR /F "delims=, tokens=1-3 eol=#" %%l IN (_conf_page.conf ) DO (
  set RESULT=
  if %%l == create (
    for /f "usebackq tokens=*" %%i in (`php "c:/wp-cli/wp-cli.phar" post list --pagename^=%%m ^| find "%%m"`) do (
      set RESULT=!RESULT!^

%%i
    )
    if "!RESULT!" == "" (
      set RESULT=!RESULT:~3,3!
      set RESULT=!RESULT: =!

      php "c:/wp-cli/wp-cli.phar" post create --post_name=%%m --post_title=%%n --post_type=page --post_status=publish
    )
  )
  if %%l == delete (
    for /f "usebackq tokens=*" %%i in (`php "c:/wp-cli/wp-cli.phar" post list --pagename^=%%m ^| find "%%m"`) do (
      set RESULT=!RESULT!^

%%i
    )
    if not "!RESULT!" == "" (
      set RESULT=!RESULT:~3,3!
      set RESULT=!RESULT: =!
      wp post delete !RESULT!
    )
  )
)

REM ##### pause #####
ECHO "[finish] setup page"
PAUSE

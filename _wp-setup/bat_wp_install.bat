@ECHO off
SETLOCAL enabledelayedexpansion
REM ################################################
REM # @title: WPインストール実行バッチファイル
REM # @description:
REM # - このファイルは基本修正しないこと
REM # - 万が一修正が必要な場合はbatファイルも修正する
REM # @author: y.ohta
REM # @rev: 1.0.0
REM ################################################
cd /d %~dp0

REM #####  .env ファイル存在確認 #####
set ENVFFILE=.\.env
if not exist %ENVFFILE% (
  echo "[error] .env is not exist."
  exit /b 1
)

REM ##### インストール実行確認 ######
SET /P ANSWER="Will you install WordPress?  (Y/N)："
if /i {%ANSWER%}=={y} (goto :yes)
echo abort.
exit
:yes

REM #####  変数読み込み #####
for /f "usebackq tokens=1,* delims==" %%a in ("%ENVFFILE%") do (
  set %%a=%%b
)

REM #####  ルートディレクトリへ移動 #####
cd ..\

REM #####  WPコアインストール実行 #####
php "c:/wp-cli/wp-cli.phar" core download --locale=ja
php "c:/wp-cli/wp-cli.phar" core config --dbname=%DBNAME% --dbuser=%DBUSER% --dbpass=%DBPASS% --dbhost=%DBHOST% --dbprefix=%DBPREFI%
php "c:/wp-cli/wp-cli.phar" core install --url=%SITEURL% --title=%SITETTL% --admin_user=%ADMINUSR% --admin_password=%ADMINPWD% --admin_email=%ADMINMAIL%

REM #####  不要プラグイン削除 #####
php "c:/wp-cli/wp-cli.phar" plugin uninstall hello

REM #####  任意プラグイン追加 #####
call .\_wp-setup\bat_wp_setup_plugin.bat

REM #####  テーマの有効化 #####
php "c:/wp-cli/wp-cli.phar" theme activate mytheme

REM #####  不要テーマの削除 #####
php "c:/wp-cli/wp-cli.phar" theme delete --all

REM #####  トップページを作成 #####
set RESULT=
for /f "usebackq tokens=*" %%i in (`php "c:/wp-cli/wp-cli.phar" post list --pagename^=home ^| find "home"`) do (
  set RESULT=!RESULT!^

%%i
)
if "!RESULT!" == "" (
  set RESULT=!RESULT:~3,3!
  set RESULT=!RESULT: =!

  php "c:/wp-cli/wp-cli.phar" post create --post_name=home --post_title=トップページ --post_type=page --post_status=publish
)

REM #####  フロントページの変更 #####
set RESULT=
php "c:/wp-cli/wp-cli.phar" option update show_on_front 'page'
for /f "usebackq tokens=*" %%i in (`php c:/wp-cli/wp-cli.phar post list --pagename^=home ^| find "home"`) do (
  set RESULT=!RESULT!^

%%i
)

set RESULT=!RESULT:~3,3!
set RESULT=!RESULT: =!
php "c:/wp-cli/wp-cli.phar" option update page_on_front !RESULT!

REM ##### デフォルトの「プライバシーポリシー」ページを削除 ######
SET RESULT=""
for /f "usebackq tokens=*" %%i in (`php c:/wp-cli/wp-cli.phar post list --post_type^=page --post_status^=draft^,publish --name^=privacy-policy ^| find "privacy-policy"`) do (
  set RESULT=!RESULT!^

%%i
)
set RESULT=!RESULT:~3,3!
set RESULT=!RESULT: =!
php "c:/wp-cli/wp-cli.phar" post delete !RESULT!

REM ##### 初期下層ページを作成 ######
call .\_wp-setup\bash_wp_setup_page.bat

REM #####  オプション変更 #####
call .\_wp-setup\bat_wp_setup_option.bat

REM ##### pause #####
ECHO "[finish] WP install"
PAUSE

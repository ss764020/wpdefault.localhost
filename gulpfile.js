var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var notify  = require('gulp-notify')
var rename = require('gulp-rename');

var sass_paths = {
  'scss': './wp-content/themes/mytheme/scss/',
  'css': './wp-content/themes/mytheme/css/'
}

// sass
gulp.task('sass', function() {
  return gulp.src(sass_paths.scss + '**/*.scss')
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "SASS compile error!",
        message: "Error: <%= error.message %>"
      })
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed',
      errLogToConsole: false
    }))
    .pipe(autoprefixer())
    .pipe(rename({extname: '.min.css'})) // rename hoge.min.css
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(sass_paths.css));
});

// build
gulp.task('build', gulp.task('sass'));

// watch
gulp.task('watch', function() {
  gulp.watch( sass_paths.scss + '**/*.scss', gulp.task('sass'));
});

// default
gulp.task('default', gulp.parallel('sass', 'watch'));

</main>
  <footer class="footer">
    <p class="footer__copy">
      <small>
        Copyright&nbsp;&copy;&nbsp;<?php echo date('Y'); ?>&nbsp;Yusuke&nbsp;Ohta&nbsp;All&nbsp;Rights&nbsp;Reserved.
      </small>
    </p>
  </footer>
  <?php wp_footer(); ?>
</body>
</html>

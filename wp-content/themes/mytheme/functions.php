<?php

if ( ! function_exists( 'my_theme_default_setup' ) ) :
  function my_theme_default_setup() {
    //---------------------
    // フロントエンドUI
    //---------------------

    //Include css/js
    function my_include_files() {
      wp_enqueue_script( 'jquery' );
      wp_enqueue_script( 'my-common', get_template_directory_uri() . '/js/common.js', array(), '20171018', true );
      wp_enqueue_style( 'my-bootstrap-grid-css', get_template_directory_uri() . '/css/bootstrap-grid.min.css', "", '20210313' );
      wp_enqueue_style( 'my-style', get_template_directory_uri() . '/css/style.min.css', "", '20171018' );
    }
    add_action( 'wp_enqueue_scripts', 'my_include_files' );

    // SEOタグ
    function the_seo($post) {
      $hd = '';
      $ogp = '';
      $seo = '';

      // - ヘッダ
      if (is_single()) {
        $hd .= '<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">';
      } else {
        $hd .= '<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">';
      }

      // - SEO
      if ( is_archive() || get_post_meta($post->ID, 'noindex', true) ) {
        $seo .= '<meta name="robots" content="noindex">';
      }

      // - OGP
      $ogp .= '<meta property="og:locale" content="ja_JP">';
      //$ogp .= '<meta property="fb:app_id" content="213475055740471">';
      $ogp .= '<meta property="og:site_name" content="' . get_bloginfo('name') . '">';

      $str = $post->post_content;
      $searchPattern = '/<img.*?src=(["\'])(.+?)\1.*?>/i';
      if (is_single()){
        if (has_post_thumbnail()){
          $image_id = get_post_thumbnail_id();
          $image = wp_get_attachment_image_src( $image_id, 'full');
          $ogp .= '<meta property="og:image" content="' . $image[0] . '">';
        } else if ( preg_match( $searchPattern, $str, $imgurl )){
          $ogp .= '<meta property="og:image" content="' . $imgurl[2] . '">';
        } else {
          $ogp .= '<meta property="og:image" content="' . esc_url( get_template_directory_uri() ) . '/images/common/ogp-image.png">';
        }
      } else {
        $ogp .= '<meta property="og:image" content="' . esc_url( get_template_directory_uri() ) . '/images/common/ogp-image.png">';
      }
      //$ogp .= '<meta name="twitter:card" content="summary">';
      //$ogp .= '<meta name="twitter:site" content="@kerger2">';

      // - SEO / OGP
      if (is_single()){
        if(have_posts()): while(have_posts()): the_post();
          $ogp .= '<meta property="og:title" content="' . get_the_title() . '">';
          $ogp .= '<meta property="og:description" content="' . mb_substr(get_the_excerpt(), 0, 100) . '">';
          $seo .= '<meta name="description" content="' . mb_substr(get_the_excerpt(), 0, 100) . '">';
          $ogp .= '<meta property="og:url" content="' . get_the_permalink() . '">';
          $ogp .= '<meta property="og:type" content="article">';
          //$ogp .= '<meta property="article:author" content="https://www.facebook.com/ss764020">';
        endwhile; endif;
      } else {
        $ogp .= '<meta property="og:title" content="' . get_bloginfo('name') .'">';
        $ogp .= '<meta property="og:description" content="' . get_bloginfo('description') . '">';
        $seo .= '<meta name="description" content="' . get_bloginfo('description') . '">';
        $ogp .= '<meta property="og:url" content="' . get_bloginfo('url') . '">';
        $ogp .= '<meta property="og:type" content="website">';
      }
      echo $hd . $seo . $ogp;
    }

    //titleタグ
    add_theme_support( 'title-tag' );

    // アイキャッチ画像
    add_theme_support( 'post-thumbnails' );

    //html5対応
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );
  }

  //google analytics
  /*
  function my_js_function_foot() {
    echo
    "<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-xxxxxxxxx', 'auto');
      ga('send', 'pageview');

    </script>";
  }
  add_action('wp_footer', 'my_js_function_foot', 99999999);
  */


  /**
  * ページネーション出力関数
  * $paged : 現在のページ
  * $pages : 全ページ数
  * $range : 左右に何ページ表示するか(-1で全件表示)
  * $show_only : 1ページしかない時に表示するかどうか(true:強制表示 / false:非表示)
  */
  function pagination( $pages, $paged, $range = 2, $show_only = false ) {

    $pages = ( int ) $pages;    //float型で渡ってくるので明示的に int型 へ
    $paged = $paged ?: 1;       //get_query_var('paged')をそのまま投げても大丈夫なように

    //表示テキスト
    $text_first   = '<i class="fas fa-angle-double-left"></i>';
    $text_before  = '<i class="fas fa-angle-left"></i>';
    $text_next    = '<i class="fas fa-angle-right"></i>';
    $text_last    = '<i class="fas fa-angle-double-right"></i>';

    if ( $show_only && $pages === 1 ) {
      // １ページのみで表示設定が true の時
      echo '<div class="newspagination"><span class="current pager">1</span></div>';
      return;
    }

    if ( $pages === 1 ) return;    // １ページのみで表示設定もない場合

    if ( 1 !== $pages ) {
      //２ページ以上の時
      echo '<div class="newspagination">';
      if($range !== -1) {
        if ( $paged > $range + 1 ) {
          // 「最初へ」 の表示
          echo '<a href="', get_pagenum_link(1) ,'" class="newspagination__item controler first">', $text_first ,'</a>';
        }
        if ( $paged > 1 ) {
          // 「前へ」 の表示
          echo '<a href="', get_pagenum_link( $paged - 1 ) ,'" class="newspagination__item controler prev">', $text_before ,'</a>';
        }
      }
      for ( $i = 1; $i <= $pages; $i++ ) {
        if (
          ($range !== -1  && $i <= $paged + $range && $i >= $paged - $range)
          || $range === -1
        ) {
          // $paged +- $range 以内であればページ番号を出力
          if ( $paged === $i ) {
            echo '<span class="newspagination__item pager current">', $i ,'</span>';
          } else {
            echo '<a href="', get_pagenum_link( $i ) ,'" data-page="' , $i , '" class="newspagination__item pager">', $i ,'</a>';
          }
        }

      }
      if($range !== -1) {
        if ( $paged < $pages ) {
          // 「次へ」 の表示
          echo '<a href="', get_pagenum_link( $paged + 1 ) ,'" class="newspagination__item controler next">', $text_next ,'</a>';
        }
        if ( $paged + $range < $pages ) {
          // 「最後へ」 の表示
          echo '<a href="', get_pagenum_link( $pages ) ,'" class="newspagination__item controler last">', $text_last ,'</a>';
        }
      }
      echo '</div>';
    }
  }

  function get_arround_post_id() {
    global $post;
    $post_tmp = $post;
    $count_post = wp_count_posts( 'post' ) -> publish;
    $postid = $post->ID;
    $previd = $postid;
    $nextid = $postid;

    //　前の記事を取得
    // ※ 最悪処理がこけても無限ループに陥らないように、全投稿数がループ回数のマックス
    for($i=0; $i < $count_post; $i++) {
      // 一つ前の記事を取得
      $post = get_previous_post()->ID;
      // 一つ前の記事の投稿フォーマットを取得
      $postformat = get_post_format($post);
      $previd = $post;

      // 前記事のIDが現在のIDと一致する、もしくは前記事の投稿フォーマットが「標準」であれば処理を抜ける
      if($previd == $postid) {
        // 前記事のIDが現在のIDと一致する場合、前記事は存在しない
        $previd = false;
        break;
      } elseif (!$postformat) {
        // 前記事の投稿フォーマットが「標準」であれば、そのIDを返却
        break;
      }
    }

    $post = $post_tmp; //初期化

    //　次の記事を取得
    // ※ 最悪処理がこけても無限ループに陥らないように、全投稿数がループ回数のマックス
    for($i=0; $i < $count_post; $i++) {
      // 一つ前の記事を取得
      $post  = get_next_post()->ID;
      // 一つ前の記事の投稿フォーマットを取得
      $postformat = get_post_format($post);
      $nextid = $post;

      // 前記事のIDが現在のIDと一致する、もしくは前記事の投稿フォーマットが「標準」であれば処理を抜ける
      if($nextid == $postid) {
        // 次記事のIDが現在のIDと一致する場合、次記事は存在しない
        $nextid = false;
        break;
      } elseif (!$postformat) {
        // 次記事の投稿フォーマットが「標準」であれば、そのIDを返却
        break;
      }
    }

    $post = $post_tmp; //初期化

    return array(
      'prev_id' => $previd,
      'next_id' => $nextid
    );
  }

  //---------------------
  // フロントエンド機能
  //---------------------

  // 共通テンプレートパーツにてアンカーリンクを張る際、ページ内リンクかサイト内リンクかで出力を分ける関数
  function anchorurl($targetslug, $anc_id) {
    global $post;
    $nowslug = $post->post_name;
    $ancurl = ($targetslug == $nowslug) ? '#'.$anc_id : get_permalink(get_page_by_path($targetslug)->ID).'?id='.$anc_id;
    return $ancurl;
  }

  //Contact form7 メールアドレス確認
  /*
  add_filter( 'wpcf7_validate_email', 'wpcf7_text_validation_filter_extend', 11, 2 );
  add_filter( 'wpcf7_validate_email*', 'wpcf7_text_validation_filter_extend', 11, 2 );
  function wpcf7_text_validation_filter_extend( $result, $tag ) {
    $type = $tag['type'];
    $name = $tag['name'];
    $_POST[$name] = trim( strtr( (string) $_POST[$name], "\n", " " ) );
    if ( 'email' == $type || 'email*' == $type ) {
      if (preg_match('/(.*)_confirm$/', $name, $matches)){
        $target_name = $matches[1];
        if ($_POST[$name] != $_POST[$target_name]) {
          if (method_exists($result, 'invalidate')) {
            $result->invalidate( $tag,"確認用のメールアドレスが一致していません");
          } else {
            $result['valid'] = false;
            $result['reason'][$name] = '確認用のメールアドレスが一致していません';
          }
        }
      }
    }
    return $result;
  }
  */

  //抜粋の文字数
  /*
  add_filter('excerpt_mblength', 'new_excerpt_mblength');
  function new_excerpt_mblength($length) {
    return 50;
  }
  */

  //抜粋の末尾の文字
  /*
  add_filter('excerpt_more', 'new_excerpt_more');
  function new_excerpt_more($more) {
    return '…';
  }
  */

  //トップページを固定ページとした時$pagedが取得されない問題を解決
  /*
  add_action( 'parse_query', 'my_parse_query' );
  function my_parse_query( $query ) {
    if ( ! isset( $query->query_vars['paged'] ) && isset( $query->query_vars['page'] ) )
      $query->query_vars['paged'] = $query->query_vars['page'];
  }
  */

  //Authorアーカイブページを無効にする
  /*
  add_action( 'template_redirect', 'theme_slug_redirect_author_archive' );
  function theme_slug_redirect_author_archive() {
      if (is_author() ) {
          wp_redirect( home_url());
          exit;
      }
  }
  */

  // 投稿アーカイブページのURL変更
  /*
  add_filter( 'register_post_type_args', 'post_has_archive', 10, 2 );
  add_filter( 'post_type_archive_link', 'post_archive_link', 10, 2 );
  function post_has_archive( $args, $post_type ) {
    global $wp_rewrite;
    if ( 'post' === $post_type && ! is_null( $wp_rewrite ) ) {
      $archive_slug = 'news';
      // Setting 'has_archive' ensures get_post_type_archive_template() returns an archive.php template.
      $args['has_archive'] = $archive_slug;
      // We have to register rewrite rules, because WordPress won't do it for us unless $args['rewrite'] is true.
      $archive_slug = $wp_rewrite->root . $archive_slug;
      add_rewrite_rule( "{$archive_slug}/?$", "index.php?post_type=$post_type", 'top' );
      $feeds = '(' . trim( implode( '|', $wp_rewrite->feeds ) ) . ')';
      add_rewrite_rule( "{$archive_slug}/feed/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
      add_rewrite_rule( "{$archive_slug}/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
      add_rewrite_rule( "{$archive_slug}/{$wp_rewrite->pagination_base}/([0-9]{1,})/?$", "index.php?post_type=$post_type" . '&paged=$matches[1]', 'top' );
    }
    return $args;
  }
  function post_archive_link( $args, $post_type ) {
    if ( 'post' === $post_type ) {
      $link = home_url( 'news/' );
    }
    return $link;
  }
  */

  // カスタムフィールド設定（Advanced Custom Fieldプラグインにて生成エクスポート）
  /* （ここにペースト）
  if( function_exists('acf_add_local_field_group') ):
    ...
  endif;
  */
  // （カスタムフィールド設定ここまで）

  //---------------------
  // バックエンドUI
  //---------------------

  // ログイン画面 CSS・js
  /*
  add_action( 'login_enqueue_scripts', 'custom_login_css' );
  function custom_login_css() {
    $files = '
      <link rel="stylesheet" href="'.get_bloginfo('template_directory').'/css/login.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="' .get_bloginfo('template_directory'). '/js/login.js"></script>
    ';
    echo $files;
  }
  */

  // 管理画面 CSS・js
  /*
  add_action( 'admin_enqueue_scripts', 'custom_admin_css' );
  function custom_admin_css() {
    $files = '
      <link rel="stylesheet" href="'.get_bloginfo('template_directory').'/css/admin.css" />
    ';
    echo $files;
  }
  */

  //ビジュアルエディタcss
  /*
  add_editor_style("./css/vis_editor.css");
  */

  //wysiwygエディタの「見出し１」～「見出し２」を削除する
  /*
  function custom_editor_settings( $initArray ){
    $initArray['block_formats'] = "段落=p; 見出し3=h3; 見出し4=h4;";
    return $initArray;
  }
  add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );
  */

  //---------------------
  // バックエンド機能
  //---------------------

  //アイキャッチを必須入力
  /*
  add_action( 'admin_head-post-new.php', 'post_edit_required' );
  add_action( 'admin_head-post.php', 'post_edit_required' );
  function post_edit_required() {
  ?>
  <script type="text/javascript">
  jQuery(function($) {
    if( 'event' == $('#post_type').val() ) {
      $('#post').submit(function(e) {
        // アイキャッチ
        if( $('#set-post-thumbnail img').length < 1 ) {
          alert('アイキャッチ画像を設定してください');
          $('.spinner').css('visibility', 'hidden');
          $('#publish').removeClass('button-primary-disabled');
          $('#set-post-thumbnail').focus();
          return false;
        }
      });
    }
  });
  </script>
  <?php
  }
  */

  //一部カスタム投稿ページではエディタを利用できないようにする
  /*
  add_action( 'init' , 'my_remove_post_editor_support' );
  function my_remove_post_editor_support() {
   remove_post_type_support( 'dlm_download', 'editor' );
  }
  */


  //投稿機能から「カテゴリー」,「タグ」を削除
  /*
  add_action('init', 'my_unregister_taxonomies');
  function my_unregister_taxonomies() {
    global $wp_taxonomies;
    if (!empty($wp_taxonomies['category']->object_type)) {
      foreach ($wp_taxonomies['category']->object_type as $i => $object_type) {
        if ($object_type == 'post') {
          unset($wp_taxonomies['category']->object_type[$i]);
        }
      }
    }
    if (!empty($wp_taxonomies['post_tag']->object_type)) {
      foreach ($wp_taxonomies['post_tag']->object_type as $i => $object_type) {
        if ($object_type == 'post') {
          unset($wp_taxonomies['post_tag']->object_type[$i]);
        }
      }
    }
    return true;
  }
  */


  //管理画面でもTerm Orderで設定した並び順に
  /*
  add_filter('get_terms_orderby', 'my_get_terms_orderby', 10);
  function my_get_terms_orderby($orderby){
    if(is_admin()){
      return "t.term_order";
    }else{
      return $orderby;
    }
  }
  */

  // get_template_part()関数の変数を渡せるバージョン、get_template_part_with_var_array()を定義
  function get_template_part_with_var_array($template_name, $var_array=null){
    if (isset($var_array)) {
      foreach($var_array as $key=>$val){
        set_query_var($key, $val);
      }
    }
    get_template_part($template_name);
  }

  //---------------------
  // パーミッション
  //---------------------

  // (管理者以外)管理画面のサイドバーを一部非表示
  /*
  add_action('admin_menu', 'remove_menus', 9999);
  function remove_menus () {
    if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
      //remove_menu_page('all-in-one-seo-pack/aioseop_class.php'); //All in One SEO
      remove_menu_page('wpcf7'); //Contact Form 7
      //remove_menu_page('metaslider'); //MetaSlider
      //remove_menu_page('edit.php?post_type=acf'); //AdvancedCustomField
      //remove_menu_page('cpt_main_menu'); //CustomPostTypeUI

      global $menu;
      //unset($menu[2]); // ダッシュボード
      //unset($menu[4]); // メニューの線1
      //unset($menu[5]); // 投稿
      //unset($menu[10]); // メディア
      //unset($menu[15]); // リンク
      unset($menu[20]); // ページ
      unset($menu[25]); // コメント
      //unset($menu[59]); // メニューの線2
      //unset($menu[60]); // テーマ
      //unset($menu[65]); // プラグイン
      unset($menu[70]); // プロフィール
      unset($menu[75]); // ツール
      //unset($menu[80]); // 設定
      //unset($menu[90]); // メニューの線3
    }
  }
  */

  /*
  // 管理バーの項目を削除
  function remove_bar_menus( $wp_admin_bar ) {
    if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
      $wp_admin_bar->remove_menu( 'wp-logo' );      // ロゴ
      //$wp_admin_bar->remove_menu( 'site-name' );    // サイト名
      //$wp_admin_bar->remove_menu( 'view-site' );    // サイト名 -> サイトを表示
      //$wp_admin_bar->remove_menu( 'dashboard' );    // サイト名 -> ダッシュボード (公開側)
      //$wp_admin_bar->remove_menu( 'themes' );       // サイト名 -> テーマ (公開側)
      //$wp_admin_bar->remove_menu( 'customize' );    // サイト名 -> カスタマイズ (公開側)
      $wp_admin_bar->remove_menu( 'comments' );     // コメント
      //$wp_admin_bar->remove_menu( 'updates' );      // 更新
      //$wp_admin_bar->remove_menu( 'view' );         // 投稿を表示
      //$wp_admin_bar->remove_menu( 'new-content' );  // 新規
      //$wp_admin_bar->remove_menu( 'new-post' );     // 新規 -> 投稿
      //$wp_admin_bar->remove_menu( 'new-media' );    // 新規 -> メディア
      //$wp_admin_bar->remove_menu( 'new-link' );     // 新規 -> リンク
      //$wp_admin_bar->remove_menu( 'new-page' );     // 新規 -> 固定ページ
      //$wp_admin_bar->remove_menu( 'new-user' );     // 新規 -> ユーザー
      //$wp_admin_bar->remove_menu( 'my-account' );   // マイアカウント
      //$wp_admin_bar->remove_menu( 'user-info' );    // マイアカウント -> プロフィール
      //$wp_admin_bar->remove_menu( 'edit-profile' ); // マイアカウント -> プロフィール編集
      //$wp_admin_bar->remove_menu( 'logout' );       // マイアカウント -> ログアウト
      $wp_admin_bar->remove_menu( 'search' );       // 検索 (公開側)
    }
  }
  add_action('admin_bar_menu', 'remove_bar_menus', 201);
  */

  //(管理者以外)all in one seo packのメタボックス消す
  /*
  add_action('admin_menu', 'remove_plugins_metabox', 1000 );
  function remove_plugins_metabox() {
    if(!current_user_can('level_10')) {
      remove_meta_box( 'aiosp', 'post', 'advanced' );
      remove_meta_box( 'aiosp', 'faq', 'advanced' );
      remove_meta_box( 'aiosp', 'event', 'advanced' );
      remove_meta_box( 'aiosp', 'tips', 'advanced' );
    }
  }
  */

  // (管理者以外)管理者ページのアクセス禁止
  /*
  add_action( 'auth_redirect', 'subscriber_go_to_home' );
  function subscriber_go_to_home( $user_id ) {
    $user = get_userdata( $user_id );
    if ( !$user->has_cap( 'edit_posts' ) ) {
      wp_redirect( get_home_url() );
      exit();
    }
  }
  */

  // (管理者以外)ログイン時ツールバーを非表示
  /*
  add_filter( 'show_admin_bar' , 'my_function_admin_bar');
  function my_function_admin_bar($content) {
    return ( current_user_can("administrator") ) ? $content : false;
  }
  */
endif;
add_action( 'after_setup_theme', 'my_theme_default_setup' );

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php the_seo($post); ?>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="thumbnail" content="<?php echo esc_url( get_template_directory_uri() ); ?>/images/common/h_logo.png" />
  <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/common/favicon.ico">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <header class="header">

  </header>
  <main class="main">

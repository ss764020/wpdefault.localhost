<?php get_header(); ?>
	<article class=sec>
    <div class="sec__cont">
      <h1 class="sec__cont__ttl">
        404 Error！<br>
        指定されたURLは存在しませんでした。<br />
        (Page Not Found)
      </h1>
      <p class="sec__cont__desc">
        お探しのページが見つかりませんでした。<br />
        URLが間違っているか、削除された可能性があります。
      </p>
      <p class="sec__cont__lnk">
        <a href="<?php echo home_url(); ?>/">
          「<?php bloginfo(); ?>」 トップへ
        </a>
      </p>
    </div>
  </article>
<?php get_footer(); ?>

(function($){
  var $win = $(window);
  var $header = $('.page-header');
  var h_hei = $header.height();

  /*** リサイズ時動作 ***/
  $win.on('load', function () {
    resize_func();
  });
  var timer = false;
  $win.on('resize', function () {
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function() {
      resize_func();
    });
  });
  /*** // リサイズ時動作 ***/

  /*** 外部ページからのアンカーリンク対応 ***/
  $win.on('load', function () {
    var anchor = getParam('id');
    if(anchor) {
      var pos = $('#' + anchor).offset().top - h_hei;
      $("html, body").scrollTop(pos);
    }
  });
  /*** 外部ページからのアンカーリンク対応 ***/

  /*** フォームの個人/法人 切り替え ***/
  // radioクリック時
  $('.radio_formtype').on('click', function(e) {
    var targ = $(this).data('target');
    $('.rd-mailform').addClass('hide');
    $(targ).removeClass('hide');
    history.pushState(null, null, targ)
  });

  //読み込み時
  var targ = location.hash ? location.hash: '#for_one';
  $('.radio_formtype[data-target="' + targ + '"]').click();
  /*** // フォームの個人/法人 切り替え ***/

  /*** フォーム必須項目 ***/
  $('.form-input').each(function() {
    var cons = $(this).data('constraints');
    if(cons != null) {
      if(cons.indexOf('@Required') != -1) {
        $(this).parent('.form-wrap').addClass('req');
      }
    }
  });
  /*** // フォーム必須項目 ***/

  /*** カード表示ホバー ***/
  $('.list_news_card__item__link').hover(function() {
    $(this).parent('.list_news_card__item').addClass('hovered');
  }, function() {
    $(this).parent('.list_news_card__item').removeClass('hovered');
  });
  /*** カード表示ホバー ***/

  /*** functions ***/

  // resize_func
  function resize_func() {
    h_hei = $header.height();
  }


  // URLのパラメータ（クエリ文字列）の特定のキーの値を取得する関数
  function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
  /*** // function ***/
})(jQuery);

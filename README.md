# ■■■ 接続情報 ■■■
## テスト環境
### 【フロントエンド】  
**URL：**  
https://test.example.com  

**ベーシック認証 ID/PW：**  
test / test
  
### 【WP管理画面】  
**URL：**  
https://test.example.com/wp-admin/  
  
**ID：**  
example_editor  
  
**PW：**  
examplepasswd
## 本番環境
### 【フロントエンド】  
**URL：**  
https://example.com  
  
### 【WP管理画面】  
**URL：**  
https://test.example.com/wp-admin/  
  
**ID/PW：**  
\- 非掲載 -

***

# ■■■　開発の手引き ■■■
## I. 開発環境構築手順
### 0. 前提条件

#### 動作環境

* Apache または Nginx
* PHP 7.4以上
* MySQL 5.6 または MariaDB 10.1以上
* Node.js
* npm（6.14.10 にて動作確認済み）

#### 事前準備

(1)  
本プロジェクト用のDBを作成  
  
(2)  
本GitリポジトリをPULL  
  
(3)  
`/_wp-setup/`ディレクトリ内、`.env.sample`を複製し、同階層に`.env`として保存、各変数を適宜編集

※  
WindowsでXAMPPを利用しての作業の場合、PHPとMySQLにパス通しておく  
参考URL：  
https://webkaru.net/dev/xampp-php-path/  
  
上記URLを参考に、環境変数の編集画面を開き、ユーザ環境変数「Path」に**PHPおよびMySQLへのパス**を追加する  
  
* PHPへのパス：  
`C:\xampp\php`  
* MySQLへのパス：  
`C:\xampp\mysql\bin`

（CドライブにXAMPPをインストールしている場合を想定）

***

### 1. wp-cliをインストール（できていない場合のみ実施）

#### (0) WP-CLIとは？
WordPressをコマンドインターフェース（黒画面）で操作するためのアプリケーション。  
  
WordPress案件にてGit管理を円滑にするために採用しているので、WordPress案件を触るときにはインストールが必要。    
  
例えば、WordPressでの設定変更をGit管理しようと思うと、設定値はファイルではなくDBに書きこまれるので、そのままでは管理ができない。  
また、WordPressプラグインは実ファイルがあるので一見Git管理できるように思えるが、WP管理画面からプラグインをアップデートした際にバージョン不整合を起こしてしまうことになるため、Git上ではどのプラグインをインストールしているのかだけを管理して、実ファイルはGitに入れたくない。（その方がGitリポジトリの容量的にも有利）  
そこで、設定変更やプラグインインストールをするためのコマンドをGit管理することで、全員の環境をそろえられるようにするべく、本プロジェクトではこのWP-CLIを採用している。  


#### (1) Windowsコマンドプロンプト環境の場合
###### ① 下記からZipファイルをダウンロード
https://bitbucket.org/ss764020/snippet-bat-install_wp_cli/downloads/
  
###### ② Zipファイルを適当な場所に解凍し、install.batファイルをダブルクリックして実行
（※ bat実行後、Zipファイルおよび解凍したディレクトリは削除してかまわない）  
  
###### ③ `C:\wp-cli` ディレクトリにパスを通す
  
パスの通し方（参考記事URL）：  
https://qiita.com/shuhey/items/7ee0d25f14a997c9e285
  
***
  
#### (2) Linux環境の場合  
###### ① インストール実行

下記コマンド実行：  

```bash
$ curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
$ chmod +x wp-cli.phar
$ sudo mv wp-cli.phar /usr/local/bin/wp
```

###### ② インストール確認

下記コマンド実行：  

```bash
$ wp --info
```

***

### 2. WordPressをインストール

* Windows環境であれば、WPインストール実行用batファイルを用意している  
→ 「A. Windows環境の場合」
* Windows以外の環境（もしくは何かしらの理由でbatを使いたくない場合）であれば、シェルファイル実行  
→ 「B. Windows以外の環境の場合」

##### A. Windows環境の場合

`/_wp-setup/bat_wp_install.bat`  
をダブルクリックして実行  
（※ 前述の「事前準備(3)」にて `.env` ファイルの作成を済ませていることが前提！）  
  

##### B. Windows以外の環境の場合

下記コマンド実行：  
（※ 前述の「事前準備(3)」にて `.env` ファイルの作成を済ませていることが前提！）  

```bash
$ bash 【プロジェクトのドキュメントルート】/_wp-setup/bash_wp_install.sh
```

### 3. SASSコンパイル環境構築

#### (1) npm-gulpをグローバルインストール（できていない場合のみ実施）

下記コマンド実行：

```bash
$ npm install -g gulp
```

#### (2) npmパッケージインストール
（※ Windows環境では後述のBATファイル実行時にコマンドが実行されるので、省略可）  
下記コマンド実行：
```bash
$ cd 【プロジェクトのドキュメントルート】/wp-content/themes/mytheme/
$ npm install
```

## II. 開発実施

#### (1) 注意点

* 修正はテーマディレクトリ（/wp-content/themes/mytheme/）内でしか行わないこと
* テキストエディタは .editorconfig が使用できるものを使うこと
* headタグ内の修正はfunctions.phpを用いて行うこと
* プラグインインストールに関しては後述の方法で実施すること

#### (2) SASSコンパイル方法

* Windows環境であれば、batファイルを用意している  
→ 「A. Windows環境の場合」
* Windows以外の環境（もしくはbatを使いたくない場合）であれば、シェルコマンド実行  
→ 「B. Windows以外の環境の場合」

##### A. Windows環境の場合

プロジェクトのドキュメントルート直下の  
`sass_convert.bat`  
をダブルクリック  
（※ 以下のような動作をする）

1. コマンドプロンプトが立ち上がる
1. ローカルnpmパッケージのインストール（もしくはアップデート）が走る
1. 一度コンパイルが走る
1. 自動監視によりSCSSファイル編集時にコンパイル実行、結果をトースト通知

→ ウィンドウを閉じると処理も止まる

##### B. Windows以外の環境の場合

下記でSASSコンパイル実行

```bash
$ cd 【プロジェクトのドキュメントルート】
$ gulp sass
```

もしくは、下記でSCSSファイルを自動監視しコンパイル実行、結果通知

```bash
$ cd 【プロジェクトのドキュメントルート】
$ gulp
```

#### (3) WP プラグインインストール

プラグインインストール（もしくはアンインストール）時は、下記の通り作業する  
（※ 自分の環境でだけでインストールしたいプラグインに関してはこの限りではない）

##### ① '_conf_plugin.conf' ファイルに、プラグインをインストールもしくはアンインストールするための命令を下記のとおり記述

```bash
##### install & activate #####
install 【プラグインスラッグ】 --activate
##### uninstall #####
uninstall 【プラグインスラッグ】
```

※ 【プラグインスラッグ】の検索方法（実行例）：

```bash
$ cd  【プロジェクトのドキュメントルート】
$ wp plugin search "advanced custom fields"   # "advanced custom fields"プラグインを検索

Success: Showing 10 of 2205 plugins.
+---------------------------+-------------------------+--------+
| name                      | slug+                   | rating |
+---------------------------+-------------------------+--------+
| Advanced Custom Fields+   | advanced-custom-fields  | 98     |
| ACF: Better Search+       | acf-better-search+      | 98     |
| Advanced Forms++          | advanced-forms          | 100    |
+---------------------------+-------------------------+--------+
```

##### ② 下記作業実行により反映

###### A. Windows環境の場合

`/_wp-setup/bat_wp_setup_plugin.bat`  
をダブルクリックして実行

###### B. Windows以外の環境の場合

```bash
$ bash 【プロジェクトのドキュメントルート】/_wp-setup/bash_wp_setup_plugin.sh
```

#### (4) WP 設定変更

WordPressの設定変更が発生した際は、下記の通り作業すること  
（※ 自分の環境でだけで設定変更したい際に関してはこの限りではない）

##### ① '_conf_option.conf' ファイルに、WP設定を変更するための命令を記述

例）  

```bash
option update date_format "Y.m.d"
```

※ 執筆時点（2021.3.22）で、下記に関する設定をここでしている  

* 日付 / 時間フォーマット
* パーマリンクの設定
* 1ページに表示する最大投稿数


##### ② 下記作業実行により反映

###### A. Windows環境の場合

`/_wp-setup/bat_wp_setup_option.bat`  
をダブルクリックして実行

###### B. Windows以外の環境の場合

```bash
$ bash 【プロジェクトのドキュメントルート】/_wp-setup/bash_wp_setup_option.sh
```

#### (5) WP 固定ページ設定

固定ページの追加/削除が発生した際は、下記の通り作業すること  
（※ 自分の環境でだけで追加/削除したい際に関してはこの限りではない）

##### ① '_conf_page.conf' ファイルに、ページを追加/削除するための命令を記述 

```bash
##### create page #####
create,【スラッグ】,【タイトル】
##### deletepage #####
delete,【スラッグ】
```


##### ② 下記作業実行により反映

###### A. Windows環境の場合

`/_wp-setup/bat_wp_setup_page.bat`  
をダブルクリックして実行

###### B. Windows以外の環境の場合

```bash
$ bash 【プロジェクトのドキュメントルート】/_wp-setup/bash_wp_setup_page.sh
```

#### (6) WP カテゴリ設定

カテゴリの追加/削除が発生した際は、下記の通り作業すること  
（※ 自分の環境でだけで追加/削除したい際に関してはこの限りではない）

##### ① '_conf_category.conf' ファイルに、カテゴリを追加/削除するための命令を記述 

```bash
##### create #####
#create,【カテゴリスラッグ】,【カテゴリ名】
##### delete #####
#delete,【カテゴリスラッグ】
```


##### ② 下記作業実行により反映

###### A. Windows環境の場合

`/_wp-setup/bat_wp_setup_category.bat`  
をダブルクリックして実行

###### B. Windows以外の環境の場合

```bash
$ bash 【プロジェクトのドキュメントルート】/_wp-setup/bash_wp_setup_category.sh
```
